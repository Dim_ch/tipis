﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.IO;
using System.Linq;

namespace lab1
{
    class Program
    {
        private const string inputFileName = "E:\\dimje\\Desktop\\учеба\\6СЕМЕСТР\\ТИПИС\\japan_text_small.txt";
        private const string outpuFileName = "E:\\dimje\\Desktop\\учеба\\6СЕМЕСТР\\ТИПИС\\japan_text_out.txt";

        static void Main()
        {
            int start= 0x30A0;
            int end = 0x30FF;
            IEnumerable<char> Katakana = Enumerable.Range(start, end - start)
                                            .Select(number => Convert.ToChar(number));
            start = 0x3040;
            end = 0x309F;
            IEnumerable<char> Hiragana = Enumerable.Range(start, end - start)
                                .Select(number => Convert.ToChar(number));

            IEnumerable<char> alphabet = Katakana.Concat(Hiragana);

            const string alph = "できつゆたしはうかるなゎれこと";

            if (!File.Exists(inputFileName))
            {
                Console.WriteLine($"Не найден файл {inputFileName} в рабочем каталоге. ");
                Console.ReadKey();

                return;
            }

            try
            {
                string text = "";

                using (StreamReader reader = new StreamReader(inputFileName, System.Text.Encoding.UTF8))
                {
                    text = reader.ReadToEnd();
                }

                using (StreamWriter writer = new StreamWriter(outpuFileName, false ,System.Text.Encoding.UTF8))
                {
                    //IEnumerable<char> newText = text.Where(symbol => alph.Contains(symbol));

                    var frequency = text.Where(x => char.IsLetter(x))
                          .GroupBy(x => char.ToLower(x))
                          .Select(x => new KeyValuePair<char, double>(x.Key, (double)x.Count() / text.Length))
                          .OrderByDescending(x => x.Value);

                    foreach (var item in frequency)
                    {
                        writer.Write($"{item.Key} - {string.Format("{0:f5}", item.Value)}\n");
                    }
                    //IEnumerable<char> hiragana = Hiragana.Where(x => char.IsLetter(x))
                    //                                .Select(x => char.ToUpper(x))
                    //                                .Distinct();
                    //foreach (char ch in hiragana)
                    //{
                    //    writer.Write($"{ch}\n");
                    //}
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

}
}
