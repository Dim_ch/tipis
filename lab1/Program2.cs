﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.IO;
using System.Linq;

namespace lab2
{
    class Program2
    {
        static void Main()
        {
            string fileFrequency = Directory.GetCurrentDirectory() + "\\input_symbols_frequency.txt";
            string fileCodes= Directory.GetCurrentDirectory() + "\\codes.txt";
            string fileProbability = Directory.GetCurrentDirectory() + "\\probability.txt";
            string fileResult = Directory.GetCurrentDirectory() + "\\Result.txt";

            if (!File.Exists(fileFrequency))
            {
                Console.WriteLine($"Не найден файл {fileFrequency} в рабочем каталоге. ");
                return;
            }

            if (!File.Exists(fileCodes))
            {
                Console.WriteLine($"Не найден файл {fileCodes} в рабочем каталоге. ");
                return;
            }

            if (!File.Exists(fileProbability))
            {
                Console.WriteLine($"Не найден файл {fileProbability} в рабочем каталоге. ");
                return;
            }

            try
            {
                string text = "";
                double[,] CanalMatr, CanalMatrMerge, CanalMatrSource;
                double[] CanalMatrMergePV;
                double Entropy, EntropySource, EntropyNoise, UsefulInf, TransferSpeed, TransferTimeDigit;
                int row, col;
                string[] stringSeparators = new string[] { " ", "\r\n", "\n" };
                String[] data;

                List<(string sym, double transition)> symTransition = new List<(string, double)>(); // вероятность переходов
                List<(string sym, string code)> symCodes = new List<(string, string)>();            // коды символов
                List<(string sym, double probability)> symFrequency = new List<(string, double)>();  // вероятность встречаемости символов

                using (StreamReader readerFreq = new StreamReader(fileFrequency, System.Text.Encoding.UTF8),
                       readerCodes = new StreamReader(fileCodes, System.Text.Encoding.UTF8),
                       readerProbability = new StreamReader(fileProbability, System.Text.Encoding.UTF8))
                {

                    #region Получения частот символов
                    text = readerFreq.ReadToEnd();
                    data = text.Split(stringSeparators, StringSplitOptions.None);

                    if (data.Length % 2 != 0)
                    {
                        throw new Exception($"В файле {fileFrequency} неверные данные");
                    }

                    for (int i = 0, j = 0; i < data.Length / 2; i++, j += 2)
                    {
                        double prob;
                        if (!double.TryParse(data[j + 1], out prob))
                        {
                            throw new Exception($"Не удалось преобразовать {data[j + 1]} в тип double");
                        }
                        symFrequency.Add((data[j], prob));
                    }
                    #endregion

                    #region Получение вероятностей изменения битов
                    text = readerProbability.ReadToEnd();
                    data = text.Split(stringSeparators, StringSplitOptions.None);

                    if (data.Length % 2 != 0)
                    {
                        throw new Exception($"В файле {fileProbability} неверные данные");
                    }

                    for (int i = 0, j = 0; i < data.Length / 2; i++, j += 2)
                    {
                        double prob;
                        if (!double.TryParse(data[j + 1], out prob))
                        {
                            throw new Exception($"Не удалось преобразовать {data[j + 1]} в тип double");
                        }
                        symTransition.Add((data[j], prob));
                    }
                    #endregion

                    #region Получение символов и их кодов из файла
                    text = readerCodes.ReadToEnd();

                    data = text.Split(stringSeparators, StringSplitOptions.None);

                    if (data.Length % 2 != 0)
                    {
                        throw new Exception($"В файле {fileCodes} неверные данные");
                    }


                    for (int i = 0, j = 0; i < data.Length / 2; i++, j += 2)
                    {
                        symCodes.Add((data[j], data[j + 1]));
                    }
                    #endregion

                    // Составление канальной матрицы
                    row = symCodes.Count;
                    col = symCodes.Count + 1;
                    CanalMatr = GetCanalMatr(symCodes, symTransition);
                    CanalMatrMerge = GetCanalMatrMerge(symFrequency, CanalMatr);
                    CanalMatrMergePV = GetCanalMatrMergePv(CanalMatrMerge);
                    CanalMatrSource = GetCanalMatrSource(CanalMatrMerge, CanalMatrMergePV);
                    // Часть Б

                }

                using (StreamWriter writerResult = new StreamWriter(fileResult, false, System.Text.Encoding.UTF8))
                {
                    writerResult.WriteLine("Канальная матрица:");
                    for (int i = 0; i < symCodes.Count; ++i)
                    {
                        //writerResult.Write("{0,-5} ", symCodes[i].sym);
                        writerResult.Write(" {0}", symCodes[i].sym);
                    }
                    writerResult.Write(" ?\n");

                    for (int i = 0; i < row; ++i)
                    {
                        writerResult.Write("{0} ", symCodes[i].sym);
                        for (int j = 0; j < col; ++j)
                        {
                            //writerResult.Write("{0,5:0.0000} ", CanalMatr[i, j]);
                            writerResult.Write("{0:0.0000}", CanalMatr[i, j]);
                            if (j != col - 1) writerResult.Write(" ");
                            else writerResult.WriteLine();
                        }
                    }

                    writerResult.WriteLine("Канальная матрица объединения:");
                    for (int i = 0; i < row; ++i)
                    {
                        for (int j = 0; j < col; ++j)
                        {
                            writerResult.Write("{0,5:0.0000} ", CanalMatrMerge[i, j]);
                        }
                        writerResult.Write("{0,-2}\n", symCodes[i].sym);
                    }

                    writerResult.WriteLine("Канальная матрица приёмника:");
                    for (int i = 0; i < row; ++i)
                    {
                        for (int j = 0; j < col; ++j)
                        {
                            writerResult.Write("{0,5:0.0000} ", CanalMatrSource[i, j]);
                        }
                        writerResult.Write("{0,-2}\n", symCodes[i].sym);
                    }

                    TransferTimeDigit = GetTransferTimeDigit(0.0, 10.0, "Введите время передачи 1 разряда");

                    Entropy = GetEntropySource(CanalMatrMergePV);
                    EntropySource = GetEntropy(symFrequency);
                    EntropyNoise = GetEntropyNoise(GetMatrNoise(CanalMatr), symFrequency);
                    UsefulInf = Entropy - EntropyNoise;
                    TransferSpeed = 1000 / (TransferTimeDigit * 4) * UsefulInf;
                    writerResult.WriteLine("Энтропия источника информации: {0:0.0000}", EntropySource);
                    writerResult.WriteLine("Энтропия приемника информации: {0:0.0000}", Entropy);
                    writerResult.WriteLine("Энтропия шума: {0:0.0000}", EntropyNoise);
                    writerResult.WriteLine("Утечка (ненадежность) информации: {0:0.0000}", EntropySource - UsefulInf);
                    writerResult.WriteLine("Количество полезной информации: {0:0.0000}", UsefulInf);
                    writerResult.WriteLine("Скорость передачи информации: {0:0.0000}", TransferSpeed);

                    double[] symbolsProbs = new double[symFrequency.Count];
                    double ZToZ = 0, ZToOne = 0, OneToZ = 0, OneToOne = 0;

                    for (int i = 0; i < symFrequency.Count; i++)
                    {
                        symbolsProbs[i] = symFrequency[i].probability;
                    }

                    foreach ((string sym, double prob) item in symTransition)
                    {
                        switch (item.sym)
                        {
                            case "00":
                                ZToZ = item.prob;
                                break;
                            case "01":
                                ZToOne = item.prob;
                                break;
                            case "11":
                                OneToOne = item.prob;
                                break;
                            case "10":
                                OneToZ = item.prob;
                                break;
                        }
                    }

                    BadChannel channel = new BadChannel(symbolsProbs, symFrequency.Count, ZToZ, ZToOne, OneToZ, OneToOne, 400);
                    PosteriorEntropy posteriorEntropy = new PosteriorEntropy(channel);

                    writerResult.WriteLine("Задание Б\nВероятности входных сообщений");

                    for (int i = 0; i < symFrequency.Count; i++)
                    {
                        writerResult.WriteLine("{0:0.0000} {1:0.0000}",
                            symFrequency[i].sym, posteriorEntropy.sourceProbs[i]);
                    }

                    writerResult.WriteLine("Вероятности выходных сообщений");

                    double sum = 0;
                    for (int i = 0; i < symFrequency.Count; i++)
                    {
                        sum += posteriorEntropy.recieverProbs[i];
                        writerResult.WriteLine("{0:0.0000} {1:0.0000}",
                            symFrequency[i].sym, posteriorEntropy.recieverProbs[i]);
                    }

                    writerResult.WriteLine("? {0:0.0000}", 1 - sum);

                    writerResult.WriteLine("\nКанальная матрица ");

                    for (int i = 0; i < symFrequency.Count; i++)
                    {
                        writerResult.Write($" {symFrequency[i].sym}");
                    }
                    writerResult.WriteLine();

                    for (int i = 0; i < channel.NoiseEffect.GetUpperBound(0) + 1; i++)
                    {
                        writerResult.Write($"{symFrequency[i].sym} ");
                        for (int j = 0; j < channel.NoiseEffect.GetUpperBound(1) + 1; j++)
                        {
                            writerResult.Write(String.Format("{0:f3} ", channel.NoiseEffect[i, j]));
                        }
                        writerResult.WriteLine();
                    }

                    TransferSpeed = 1000 / (TransferTimeDigit * 4) * posteriorEntropy.UsefulInformation;
                    writerResult.WriteLine("Энтропия источника информации: {0:0.0000}", posteriorEntropy.SourceEntropy);
                    writerResult.WriteLine("Энтропия приемника информации: {0:0.0000}", posteriorEntropy.ReceiverEntropy);
                    writerResult.WriteLine("Энтропия шума: {0:0.0000}", posteriorEntropy.EntropyOfNoise);
                    writerResult.WriteLine("Апостериорная энтропия: {0:0.0000}", posteriorEntropy.PosteriorChannelEntropy);
                    writerResult.WriteLine("Количество полезной информации: {0:0.0000}", posteriorEntropy.UsefulInformation);
                    writerResult.WriteLine("Скорость передачи информации: {0:0.0000}", TransferSpeed);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// Составляет канальную матрицу
        /// </summary>
        /// <param name="symCodes"></param>
        /// <param name="symTransition"></param>
        /// <returns></returns>
        static double[,] GetCanalMatr(List<(string, string)> symCodes, List<(string, double)> symTransition)
        {
            int row = symCodes.Count;
            int col = symCodes.Count + 1;
            double[,] CanalMatr = new double[row, col];
            double prob = 0, sum;

            for (int i = 0; i < row; i++)
            {
                sum = 0;
                for (int j = 0; j < row; j++)
                {
                    prob = GetProbabilityDistortion(symTransition, symCodes[i].Item2, symCodes[j].Item2);
                    sum += prob;
                    CanalMatr[i, j] = prob;
                }
                CanalMatr[i, col - 1] = 1 - sum;
            }

            return CanalMatr;
        }
        
        /// <summary>
        /// Получение канальной матрицы объединения
        /// </summary>
        /// <param name="symFrequency"></param>
        /// <param name="canalMatr"></param>
        /// <returns></returns>
        static double[,] GetCanalMatrMerge(List<(string, double)> symFrequency, double[,] canalMatr)
        {
            int row = canalMatr.GetLength(0);
            int col = canalMatr.Length / row;
            double[,] canalMatrMerge = new double[row, col];

            for (int i = 0; i < row; ++i)
            {
                for (int j = 0; j < col; ++j)
                {
                    canalMatrMerge[i, j] = canalMatr[i, j] * symFrequency[i].Item2;
                }
            }

            return canalMatrMerge;
        }

        static double[] GetCanalMatrMergePv(double[,] canalMatrMerge)
        {
            int row = canalMatrMerge.GetLength(0);
            int col = canalMatrMerge.Length / row;
            double[] PV = new double[col];

            for (int i = 0; i < row; ++i)
            {
                for (int j = 0; j < col; ++j)
                {
                    PV[j] += canalMatrMerge[i, j];
                }
            }

            return PV;
        }

        static double[,] GetCanalMatrSource(double[,] canalMatrMerge, double[] canalMatrMergePv)
        {
            int row = canalMatrMerge.GetLength(0);
            int col = canalMatrMerge.Length / row;
            double[,] matrSource = new double[row, col];

            for (int i = 0; i < row; ++i)
            {
                for (int j = 0; j < col; ++j)
                {
                    matrSource[i,j] = canalMatrMerge[i, j] / canalMatrMergePv[j];
                }
            }

            return matrSource;
        }

        static double[,] GetMatrNoise(double[,] canalMatr)
        {
            int row = canalMatr.GetLength(0);
            int col = canalMatr.Length / row;
            double[,] matr = new double[row, col];

            for (int i = 0; i < row; ++i)
            {
                for (int j = 0; j < col; ++j)
                {
                    matr[i, j] = canalMatr[i, j] * Math.Log2(canalMatr[i, j]);
                }
            }

            return matr;
        }

        /// <summary>
        /// Определяет вероятность перехода из code1 в code2
        /// </summary>
        /// <param name="code1"></param>
        /// <param name="code2"></param>
        /// <returns></returns>
        static double GetProbabilityDistortion(List<(string, double)> symTransition, 
                                               string code1, string code2) {
            double probability = 1;

            for (int i = 0; i < code1.Length; ++i)
            {
                probability *= GetTransitionProbability(symTransition, code1[i], code2[i]);
            } 

            return probability;
        }

        /// <summary>
        /// Определяет вероятность перехода бита1 в бит2
        /// </summary>
        /// <param name="symTransition"></param>
        /// <param name="ch1"></param>
        /// <param name="ch2"></param>
        /// <returns></returns>
        static double GetTransitionProbability(List<(string sym, double probability)> symTransition, char ch1, char ch2)
        {
            double probability = 0;

            for (int i = 0; i < symTransition.Count; i++)
            {
                if (symTransition[i].sym[0] == ch1 && symTransition[i].sym[1] == ch2)
                {
                    probability = symTransition[i].probability;
                    break;
                }
            }

            return probability;
        }

        /// <summary>
        /// Энтропия источника информации
        /// </summary>
        /// <param name="symFrequency"></param>
        /// <returns></returns>
        static double GetEntropy(List<(string sym, double freq)> symFrequency)
        {
            double probability = 0;

            for (int i = 0; i < symFrequency.Count; i++)
            {
                probability += symFrequency[i].freq * Math.Log2(symFrequency[i].freq);
            }

            return probability * -1;
        }

        static double GetEntropySource(double[] canalMatrMergePv)
        {
            double probability = 0;

            for (int i = 0; i < canalMatrMergePv.Length; i++)
            {
                probability += canalMatrMergePv[i] * Math.Log2(canalMatrMergePv[i]);
            }

            return probability * -1;
        }

        static double GetEntropyNoise(double[,] matrNoise, List<(string sym, double freq)> symFrequency)
        {
            int row = matrNoise.GetLength(0);
            int col = matrNoise.Length / row;
            double[] arr = new double[row];
            double entropyNoise = 0;

            for (int i = 0; i < row; ++i)
            {
                for (int j = 0; j < col; ++j)
                {
                    arr[i] += matrNoise[i, j];
                }
                arr[i] *= (symFrequency[i].freq * -1);
                entropyNoise += arr[i];
            }

            return entropyNoise;
        }

        /// <summary>
        /// Считывает с консоли время передачи одного бита
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="mes"></param>
        /// <returns></returns>
        static double GetTransferTimeDigit(double from, double to, string mes)
        {
            string line;
            double number;
            string message = String.Format("{0} от {1:0.0} до {2:0.0}: ", mes, from, to);
            do
            {
                Console.Write(message);
                line = Console.ReadLine();

                while (!double.TryParse(line, out number))
                {
                    Console.Write(message);
                    line = Console.ReadLine();
                }

            } while ((number < from) || (number > to));

            return number;
        }

    }
}
